//Jeremy Okeyo - jokeyo2@jhu.edu
//Marcelo Morales - lmoral10@jhu.edu
//The mirrorv plugin generates a mirror image of the input image, 
//with all pixels being reflected vertically. It does not take 
//any command line parameters.


#include <stdlib.h>
#include <stdio.h>
#include "image_plugin.h"
#include <iostream>

struct Arguments {
	// This plugin doesn't accept any command line arguments;
	// just define a single dummy field.
	int dummy;
};

const char *get_plugin_name(void) {
	return "mirrorv";
}

const char *get_plugin_desc(void) {
	return "mirror image vertically";
}

void *parse_arguments(int num_args, char *args[]) {
	(void) args; // this is just to avoid a warning about an unused parameter

	if (num_args != 0) {
		return NULL;
	}
	return calloc(1, sizeof(struct Arguments));
}

// Helper function to swap the blue and green color component values.
static void mirrorv_operation(Image *out, Image *source, unsigned width, unsigned &top_row
					, unsigned &bot_row) {
	unsigned temp = 0, top_index = top_row * width, bot_index = bot_row * width;
	for (unsigned j = 0; j < width; j++) {
		temp  = source->data[top_index + j];
		out->data[top_index + j] = source->data[bot_index + j];
		out->data[bot_index + j] = temp;
	}
}

struct Image *transform_image(struct Image *source, void *arg_data) {
	struct Arguments *args = (Arguments*) arg_data;

	// Allocate a result Image
	struct Image *out = img_create(source->width, source->height);
	if (!out) {
		free(args);
		return NULL;
	}

	unsigned top_row = 0, bot_row = source->height - 1;
 unsigned mid = source->height / 2;
	while (top_row <= mid) {
		mirrorv_operation(out, source, source->width, top_row, bot_row);
		top_row++; bot_row--;
	}

	free(args);

	return out;
}
