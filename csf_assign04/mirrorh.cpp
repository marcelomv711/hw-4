//Jeremy Okeyo - jokeyo@jhu.edu
//Marcelo Morales - lmoral10@jhu.edu
// The mirrorh plugin generates a mirror image of the input image, with 
//all pixels being reflected horizontally. It does not take any 
//command line parameters.
//

#include <stdlib.h>
#include "image_plugin.h"

struct Arguments {
	// This plugin doesn't accept any command line arguments;
	// just define a single dummy field.
	int dummy;
};

const char *get_plugin_name(void) {
	return "mirrorh";
}

const char *get_plugin_desc(void) {
	return "mirror image horizontally";
}

void *parse_arguments(int num_args, char *args[]) {
	(void) args; // this is just to avoid a warning about an unused parameter

	if (num_args != 0) {
		return NULL;
	}
	return calloc(1, sizeof(struct Arguments));
}

// Helper function to swap the left pixels with its corresponding right pixel
static void mirrorh_operation(Image *out, Image *source, unsigned pxls, unsigned &left_col
					, unsigned &right_col) {
	unsigned temp = 0, mid = (right_col + 1) / 2;
	while (left_col <= mid) {
			temp  = source->data[pxls + left_col];
			out->data[pxls + left_col] = source->data[pxls + right_col];
			out->data[pxls + right_col] = temp;
			left_col++; right_col--;
	}
}

struct Image *transform_image(struct Image *source, void *arg_data) {
	struct Arguments* args = (Arguments*) arg_data;

	// Allocate a result Image
	struct Image *out = img_create(source->width, source->height);
	if (!out) {
		free(args);
		return NULL;
	}
	
	unsigned left_col = 0, right_col = 0;
	unsigned height = source->height;
	unsigned pxls = 0;

	for (unsigned i = 0; i < height; i++) {
		left_col = 0;
		right_col = source->width - 1;
		pxls = source->width * i;
		mirrorh_operation(out, source,  pxls, left_col, right_col);
	}

	free(args);

	return out;
}
