//Jeremy Okeyo - jokeyo2@jhu.edu
//Marcelo Morales - lmoral10@jhu.edu
// expose plugin changes all red/green/blue color component values by a 
//specified factor. It takes a single command line argument, which is 
//the floating point value to use as the factor. The factor must not 
//be negative. Note that if the factor is greater than 1, multiplying 
//the factor by a color component value in the original image could result in a value greater than 255. The transformation should limit all effective color component values to 255: this will cause “over-exposed” pixels to saturate towards white

#include <stdlib.h>
#include "image_plugin.h"
#include <string>

struct Arguments {
	
	float factor;
};

const char *get_plugin_name(void) {
	return "expose";
}

const char *get_plugin_desc(void) {
	return "adjust the intensity of all pixels";
}

//this takes in a an argument, which is a floating point value of a factor
void *parse_arguments(int num_args, char *args[]) {
	(void) args; // this is just to avoid a warning about an unused parameter
	if (num_args != 1) {
		return NULL;
   }

   struct Arguments *arg = (Arguments*) malloc(sizeof(Arguments));
   if ((arg->factor = std::stod(args[0])) < 0) {
		return NULL;
	}
	return arg;
}

// Helper function for the expose function -- arg is not passed in
static uint32_t expose(uint32_t pix, struct Arguments* arg) {
	uint8_t r, g, b, a;
	uint8_t r1, b1, g1;
	img_unpack_pixel(pix, &r, &g, &b, &a);
	r1 = r * arg->factor;
	b1 = b * arg->factor;
	g1 = g * arg->factor;
	if (arg->factor > 1) {
		if (r1 < r) {
			r1 = 255;
		}
		if (b1 < b) {
			b1 = 255;
		}
		if (g1 < g) {
			g1 = 255;
		}
	}

	return img_pack_pixel(r1, g1, b1, a);
}

struct Image *transform_image(struct Image *source, void *arg_data) {
	struct Arguments *args = (Arguments*) arg_data;

	// Allocate a result Image
	struct Image *out = img_create(source->width, source->height);
	if (!out) {
		free(args);
		return NULL;
	}

	unsigned num_pixels = source->width * source->height;
	for (unsigned i = 0; i < num_pixels; i++) {
		out->data[i] = expose(source->data[i], args);
	}

	free(args);

	return out;
}
