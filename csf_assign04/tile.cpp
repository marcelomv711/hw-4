//Jeremy Okeyo - jokeyo2@jhu.edu
//Marcelo Morales - lmoral10@jhu.edu
// tile plugin generates an image containing an N x N arrangement of 
//tiles, each tile being a smaller version of the original image,
// and the overall result image having the same dimensions as the 
//original image. It takes one command line parameter, an integer 
//specifying the tiling factor N.
//


#include <stdlib.h>
#include "image_plugin.h"

struct Arguments {
	// This plugin doesn't accept any command line arguments;
	// just define a single dummy field.
	int tiling_factor;
};

const char *get_plugin_name(void) {
	return "tile";
}

const char *get_plugin_desc(void) {
	return "tile source image in an NxN arrangement";
}

void *parse_arguments(int num_args, char *args[]) {
	(void) args; // this is just to avoid a warning about an unused parameter

	if (num_args != 1) {
		return NULL;
	}

	struct Arguments* arg = (Arguments*) malloc(sizeof(struct Arguments)); 
	arg->tiling_factor = atoi(args[0]);
	if (arg->tiling_factor < 1) {
		return NULL;
	}
	return arg;
}

//Duplicates tile N x N times onto return image
static void tile_operation(struct Image* out,struct Image* tile) {
	unsigned tile_width = tile->width;
	unsigned tile_height = tile->height;
	for (unsigned i = 0; i < out->height; i++) {
		for (unsigned j = 0; j < out->width; j++) {
			out->data[i * out->width + j] = tile->data[i % tile_height * tile_width + j % tile_width];
		}
	}
}

//Creates a tile based on the original image
static Image* create_tile(struct Image *source, int tiling_factor) {
	unsigned tile_width = source->width / tiling_factor;	
	unsigned tile_height = source->height / tiling_factor;	
	if ( source->width % tiling_factor!= 0) {
		tile_width += 1;			
	}
	if ( source->height % tiling_factor!= 0) { 
		tile_height += 1;
	} 

	struct Image *tile = img_create(tile_width, tile_height);
	for (unsigned i = 0; i < tile_height; i++) {
		for (unsigned j = 0; j < tile_width; j++) {
				tile->data[i * tile->width + j] = source->data[i * source->width * tiling_factor + j * tiling_factor];
		/*	if (width_excess && j == tile_width - 1) {
				tile[i * source->width + ++j] = source_data[i * source->width * tiling_factor + ++j * tiling_factor];
			}*/
		}
	}
	return tile;
}

struct Image *transform_image(struct Image *source, void *arg_data) {
	
	struct Arguments *args = (Arguments*)  arg_data;

	// Allocate a result Image
	struct Image *out = img_create(source->width, source->height);
	if (!out) {
		free(args);
		return NULL;
	}
	struct Image* tile = create_tile(source, args->tiling_factor);
	tile_operation(out, tile);
	free(args);

	return out;
}
