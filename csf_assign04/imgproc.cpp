//Jeremy Okeyo - jokeyo2@jhu.edu
//Marcelo Morales - lmoral10@jhu.edu
#include <string>
#include <stdio.h>
#include <iostream>
#include <dlfcn.h>
#include "image.h"
#include "image_plugin.h"
#include <vector> 
#include <sys/types.h>
#include <dirent.h>
#include <string.h>

using std::cout;
using std::endl;
using std::cerr;
using std::string;
using std::vector;


struct Plugin {
    void *handle;
    const char *(*get_plugin_name)(void);
    const char *(*get_plugin_desc)(void);
    void *(*parse_arguments)(int num_args, char *args[]);
    struct Image *(*transform_image)(struct Image *source, void *arg_data);
};

//Handles memory deallocation before forced exit due to error
void handle_exit(struct Plugin* plugin, string error, DIR* directory) {
	if (plugin->handle != NULL) {
		dlclose(plugin->handle);
	}
	closedir(directory);
	free(plugin);
	cerr << "Error: " << error << endl;
	exit(1);
}

//Checks whether all APIs are available
void check_API(struct Plugin* plugin, DIR* directory) {
	if (plugin->parse_arguments == NULL) {
		handle_exit(plugin, "A required image API can't be found", directory);
	} else if (plugin-> transform_image == NULL) {
		handle_exit(plugin, "A required image API can't be found", directory);
	} else if (plugin->parse_arguments == NULL) {
		handle_exit(plugin, "A required image API can't be found", directory);
	} else if (plugin-> transform_image == NULL) {
		handle_exit(plugin, "A required image API can't be found", directory);
	}
}

//Loads plugin and its methods from the provided path
int loadPlugin(struct Plugin *plugin, string path, DIR* directory) {
	plugin->handle = dlopen(path.c_str(), RTLD_LAZY);
	if (plugin->handle == NULL) {
		handle_exit(plugin, "An image processing plugin can't be loaded", directory);
	}
	*(void **) (&plugin->get_plugin_name) = dlsym(plugin->handle, "get_plugin_name");
	*(void **) (&plugin->get_plugin_desc) = dlsym(plugin->handle, "get_plugin_desc");
	*(void **) (&plugin->parse_arguments) = dlsym(plugin->handle, "parse_arguments");
	*(void **) (&plugin->transform_image) = dlsym(plugin->handle, "transform_image");
	return 0;
}

//Constructs the list fro the list command
int list (string path, struct Plugin *plugin, vector<string> &loaded_plugins, DIR* directory) {
	if (loadPlugin(plugin, path, directory) == 1) {
		return 0;
	}
	check_API(plugin, directory);	
	loaded_plugins.push_back(plugin->get_plugin_name());
	loaded_plugins.push_back(plugin->get_plugin_desc());

	dlclose(plugin->handle);
	return 1;
}

//Gets the required args
void get_args(char** args, char * argv[], int argc) {
	//args = new char*[argc - 5];
	for (int i = 0; i < argc - 5; i++) {
		args[i] = new char[20];
	}
	for (int i = 5; i < argc; i++) {
		strcpy(args[i-5], argv[i]);
	}
}

//Helper Method: deletes allocated args
void delete_args(char** args, int argc) {
	for (int i = 0; i < argc - 5; i++) {
		delete args[i];
	}
	delete [] args;
}

//Executes the specified plugin
int exec (char * argv[], int argc, string path, struct Plugin *plugin, DIR* directory) {
	loadPlugin(plugin, path, directory); 
	check_API(plugin, directory);

	char** args = new char*[argc - 5];
	get_args(args, argv, argc);
	void *arg = plugin->parse_arguments(argc - 5, args);
	delete_args(args, argc);
	if (arg == NULL) {
		handle_exit(plugin, "Invalid command arguments", directory);
	}
	struct Image *source = img_read_png(argv[3]);
	if (source == NULL) {
		handle_exit(plugin, "Invalid input file", directory);
	}
	struct Image *img = plugin->transform_image(source, arg);
	img_destroy(source);

	if (img_write_png(img, argv[4]) == 0) { 
		img_destroy(img);
		handle_exit(plugin, "Unable to write image", directory);
	}
	img_destroy(img);
	dlclose(plugin->handle);
	return 0; 
}

//Prints loaded plugins
void print_list(int handler, vector<string> loaded_plugins, int num_plugin) { 
	if (handler == 0) {
		cout << "Loaded " << num_plugin << " plugin(s)" << endl;
		for (unsigned index = 0; index < loaded_plugins.size(); index+=2) {
			printf("%8s: %s\n", loaded_plugins.at(index).c_str(), loaded_plugins.at(index + 1).c_str());
		}
	}	
}

//Checks whether file is a shared library
int check_file(string file) {
	if (file.compare(".") == 0 || file.compare("..") == 0) {
		return 1;
	} else if (file.find(".so") == string::npos) {
		return 1;
	} else {
		return 0;
	}
}

//Selects command to be performed
void commands(DIR* directory, string path, struct Plugin *plugin, int argc,
										char* argv[], int handler ) {
		struct dirent* direct = readdir(directory);
		vector<string> loaded_plugins; 
		int match = 1, num_plugin = 0;
		while (direct != NULL && match == 1) {
			string file(direct->d_name);
			if (check_file(file) == 1) {
				direct = readdir(directory);
				continue;
			}
			string specific_path = path;
			specific_path.append(file);

			if (handler == 0) {
				num_plugin += list(specific_path, plugin, loaded_plugins, directory);
			} else {
				string command = argv[2];
				command.append(".so");
				if (command.compare(direct->d_name) == 0) {
					match =  exec(argv, argc, specific_path, plugin, directory);
				}
			}
			direct = readdir(directory);
		}

		if (handler == 0) {
			print_list(handler, loaded_plugins, num_plugin);
		} else if (handler == 1 && match == 1) {
			handle_exit(plugin, "Plugin couldn't be found", directory);
		}
}

//gets the plugin directory
DIR* getDirectory(string &path) {
	char* env_dir = getenv("PLUGIN_DIR");

	if (env_dir == NULL) {
		path.append("plugins");
	   path.append("/");
		return opendir("plugins");
	} else { 
		path.append(env_dir);
		path.append("/");
		return opendir(env_dir);
	}
}

//Reads the plugins from their directory and closes direcotry after its done
int read_files(int argc, char * argv[], int handler) {
	string path = "./";
	DIR* directory = getDirectory(path);
	
	struct Plugin *plugin = (Plugin*) malloc(sizeof(Plugin));
	
	commands(directory, path, plugin, argc, argv, handler);
	free(plugin);
	closedir(directory);
	return 0;
}

int main (int argc, char * argv[]) {
	if (argc == 1) {
		cout << "Usage: imgproc <command> [<command args...>]" << endl;
		cout << "Commands are:\n  list\n  exec <plugin> <input img> <output img> [<plugin args...>]" << endl;
		return 0;
	}
	
	string command(argv[1]);
	if (command.compare("list") == 0) {
		read_files(argc, argv, 0);
	} else if (command.compare("exec") == 0) {
		read_files(argc, argv, 1);
	} else {
		cerr << "Error: Unknown command name" << endl;
		exit(1);
	}
	return 0;
}


